import {
	createMuiTheme,
	makeStyles,
	ThemeProvider,
} from "@material-ui/core/styles";

//Components
import Navbar from "../src/components/home/navbar";
import Page5 from '../src/components/team/team';
import Footer from '../src/components/home/footer';

import globalTheme from "../src/assets/theme";

const theme = createMuiTheme(globalTheme);
export default function About() {
	return(
		<ThemeProvider theme={theme}>
			<Navbar/>
			<Page5 />
			<Footer />
		</ThemeProvider>
	)
}
